====================
DarkPhotonProduction
====================


.. image:: https://img.shields.io/pypi/v/darkphotonproduction.svg
        :target: https://pypi.python.org/pypi/darkphotonproduction

.. image:: https://img.shields.io/travis/jonas.paccolat/darkphotonproduction.svg
        :target: https://travis-ci.org/jonas.paccolat/darkphotonproduction

.. image:: https://readthedocs.org/projects/darkphotonproduction/badge/?version=latest
        :target: https://darkphotonproduction.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




NLO differential cross section of dark photon hard production at fixed target proton experiments


* Free software: Apache Software License 2.0
* Documentation: https://darkphotonproduction.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
