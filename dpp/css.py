#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np
from scipy.integrate import quad
from scipy.special import zeta, jv

from dpp import settings
from dpp import pdf as PDF
from dpp import setup as SETUP

pdf = PDF.PDF(settings.pdfset)
setup = SETUP.Setup(settings.P_beam, settings.nf)

def init_para(parametrization):
    """
    Set the parameters of the non-perturbative correction.
    """

    if parametrization is None:
        parametrization = ['BLNY', (0.201, 0.184, -0.026)]
    elif parametrization == 'BLNY':
        parametrization = ['BLNY', (0.201, 0.184, -0.026)]
    elif parametrization == 'SY':
        parametrization = ['SY', (0.212, 0.84, 0.)]
    else:
        pass

    return parametrization

def get_sudakov_coefficients(mu2):

    nf = setup.nf
    epsilonQ2 = pdf.alpha.alphasQ2(mu2)/np.pi

    A1 = setup.CF
    A2 = setup.CF*(setup.NC*(67./36.-np.pi**2/12)-5./18.*nf)
    A = epsilonQ2*A1+epsilonQ2**2*A2

    B1 = -3./2.*setup.CF
    # B2 = CF**2*(np.pi**2/4.-3./16.-3.*zeta(3,1))+\
    #      CF*NC*(11./36.*np.pi**2-193./48.+3./2.*zeta(3,1))+\
    #      CF*nf*(17./24.-np.pi**2/18.)
    B = epsilonQ2*B1
    # +epsilonQ2**2*B2

    return A, B

def sudakov_integrand(mu2, dp):

    A, B = get_sudakov_coefficients(mu2)

    return (np.log(dp.M**2/mu2)*A+B)/mu2

def sudakov_exponent(dp, b, eps):
    """Returns the Sudakov exponent integral and its numerical error."""

    b0 = 2*np.exp(-np.euler_gamma)
    mu2min, mu2max = (b0/b)**2, dp.M**2

    tot = quad(sudakov_integrand, mu2min, mu2max, args=(dp,),\
                epsrel=eps, epsabs=0, limit=1000)[0]

    return tot

def P_integrand(xi, x, iParton, Q2, mem):

    Cqq1_non_singular = 2*(1-x/xi)/3
    Cqg1_non_singular = x/xi*(1-x/xi)/2

    tot = pdf.pdf(xi, iParton, Q2, mem)*Cqq1_non_singular\
        + pdf.pdf(xi, 0, Q2, mem)*Cqg1_non_singular

    return tot/xi

def P(x, iParton, b, mem, eps):
    """Returns the convoluted PDF."""

    b0 = 2*np.exp(-np.euler_gamma)
    Q2 = (b0/b)**2
    epsilonQ2 = pdf.alpha.alphasQ2(Q2)/np.pi

    args = (x, iParton, Q2, mem)
    tot = epsilonQ2*quad(P_integrand, x, 1, args=args, epsrel=eps, epsabs=0, limit=1000)[0]

    Cqq0_singular = 1.0
    Cqq1_singular = (np.pi**2-8)/3
    tot += pdf.pdf(x, iParton, Q2, mem)*(Cqq0_singular+epsilonQ2*Cqq1_singular)

    return tot

def W(dp, b, mem, eps):
    """Returns the W function."""

    tot = 0.
    for iParton in range(1, setup.nf+1):

        tot += setup.sigma0[iParton]*\
                (P(dp.xA, iParton, b, mem=mem, eps=eps/4)\
                *P(dp.xB, -iParton, b, mem=mem, eps=eps/4)\
                +P(dp.xA, -iParton, b, mem=mem, eps=eps/4)\
                *P(dp.xB, iParton, b, mem=mem, eps=eps/4))

    return tot/setup.S_beam

def F_NP(dp, b, parametrization):

    scheme, args = init_para(parametrization)
    a1, a2, a3 = args

    if scheme == 'BLNY':
        M0 = 3.2
        tot = (a1+a2*np.log(dp.M/M0)+a3*np.log(100*dp.M*dp.M/setup.S_beam))*b**2

    elif scheme == 'SY':
        M0 = np.sqrt(2.4)
        x0 = 0.01
        lam = 0.2
        bmax = 1.5
        bs = bstar(b, bmax)
        tot = a1*b**2+a2*np.log(b/bs)*np.log(dp.M/M0)\
              +a3*b**2*((x0/dp.xA)**lam+(x0/dp.xB)**lam)

    return tot

def bstar(b, bmax):
    """Returns the bounded b variable."""

    return b/np.sqrt(1+(b/bmax)**2)

def b_integrand(b, dp, mem, parametrization, eps):
    """Returns the integrand of the b integral."""

    bstar_max = 1.5
    b0 = 2*np.exp(-np.euler_gamma)
    Qmin = np.sqrt(pdf.pcentral.q2Min)

    A = sudakov_exponent(dp, bstar(b, bstar_max), eps=eps/40)
    B = W(dp, bstar(b, b0/Qmin), mem=mem, eps=eps)
    C = np.exp(-F_NP(dp, b, parametrization))

    prefactor = b*jv(0, dp.kT*b)
    tot = prefactor*np.exp(-A)*B*C

    return tot

def differential_cross_section(dp, parametrization=None, mem=0, eps=1e-3):
    """Returns the CSS differential cross section."""

    bmin=1e-4
    bmax=10.

    prefactor = setup.P_beam/dp.Ek/2
    args = (dp, mem, parametrization, eps)
    ds = quad(b_integrand, bmin, bmax, args=args, epsrel=eps, epsabs=0, limit=1000)[0]

    return prefactor*ds

def differential_cross_section_uncertainty(dp, args):

    dcs_set = list()
    for m in range(pdf.pset.size):
        dcs_set.append(differential_cross_section(dp,
                            parametrization=args['parametrization'], mem=m))
    dcs = pdf.pset.uncertainty(dcs_set, args['CL'])

    return dcs.central - dcs.errminus, dcs.central + dcs.errplus
