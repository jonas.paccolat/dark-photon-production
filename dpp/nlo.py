#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np
from scipy.integrate import quad

from dpp import kernel
from dpp import settings
from dpp import pdf as PDF
from dpp import setup as SETUP

pdf = PDF.PDF(settings.pdfset)
setup = SETUP.Setup(settings.P_beam, settings.nf)

### differential cross section

def t_limit(dp):
    """
    Boundaries of the Mandelstam variable t (at the partonic level) which is
    used as the integration variable.
    """

    tmin = dp.M**2 - dp.M*dp.MT/dp.xB
    tmax = dp.M / (dp.xA*dp.MT - dp.M) * dp.kT**2

    return tmin, tmax

def amplitude(dp, x, y, z):
    """
    Recurent expression in the NLO amplitude squared terms.
    """

    return 2*x*dp.M**2/(y*z) + y/z + z/y

def xiA(dp, t):

    return dp.xA * dp.MT/dp.M * t/(dp.kT**2 + t)

def xiB(dp, t):

    return dp.xB/dp.M/dp.MT * (dp.M**2 - t)

def ustar(dp, t):

    return (dp.M**2 - t) / (dp.kT**2 + t) * dp.kT**2

def sstar(dp, t):

    return ustar(dp, t) * t / dp.kT**2

def t_integrand(t, dp, Q2, mem):

    xiA_ = xiA(dp, t)
    xiB_ = xiB(dp, t)
    ustar_ = ustar(dp, t)
    sstar_ = sstar(dp, t)

    tot = 0.
    for iParton in range(1, setup.nf+1):

        fqq = pdf.pdf(xiA_, iParton, Q2, mem)\
              * pdf.pdf(xiB_, -iParton, Q2, mem)\
              + pdf.pdf(xiA_, -iParton, Q2, mem)\
              * pdf.pdf(xiB_, iParton, Q2, mem)
        fqg = pdf.pdf(xiA_, iParton, Q2, mem)\
              * pdf.pdf(xiB_, 0, Q2, mem)\
              + pdf.pdf(xiA_, -iParton, Q2, mem)\
              * pdf.pdf(xiB_, 0, Q2, mem)
        fgq = pdf.pdf(xiA_, 0, Q2, mem)\
              * pdf.pdf(xiB_, iParton, Q2, mem)\
              + pdf.pdf(xiA_, 0, Q2, mem)\
              * pdf.pdf(xiB_, -iParton, Q2, mem)

        tot += setup.sigma0[iParton] * pdf.alpha.alphasQ2(Q2)\
               * (32./3 * fqq * amplitude(dp, sstar_, t, ustar_)
               - 4. * fgq * amplitude(dp, ustar_, sstar_, t)
               - 4. * fqg * amplitude(dp, t, ustar_, sstar_))

    prefactor = 1 / t / (dp.M**2 - t)

    return prefactor*tot

def differential_cross_section(dp, scale=1, mem=0, eps=1e-3):

    if dp.kT == 0:
        return np.nan

    Q2 = scale * dp.M**2

    prefactor = setup.P_beam / dp.Ek / (16*np.pi*setup.S_beam)
    tmin, tmax = t_limit(dp)
    args = (dp, Q2, mem)
    dcs = quad(t_integrand, tmax, tmin, args=args,\
                         epsrel=eps, epsabs=0, limit=1000)[0]

    return prefactor * dcs

def differential_cross_section_uncertainty(dp, args=None):

    if args['type'] == 'pdf':

        dcs_set = list()
        for m in range(pdf.pset.size):
            dcs_set.append(differential_cross_section(dp, mem=m))
        dcs = pdf.pset.uncertainty(dcs_set, args['CL'])

        return dcs.central - dcs.errminus, dcs.central + dcs.errplus

    elif args['type'] == 'scale':

        scale = float(args['scale'])
        dcs = [differential_cross_section(dp, scale=scale**i)\
              for i in np.linspace(-1, 1, args['N'])]

        return float(min(dcs)), float(max(dcs))

    else:
        SyntaxError('No uncertainty parameters provided.')

### integrated cross section

def first_integrand(z, xiA, dp, Q2, mem=0):
    """First integral."""

    tau = dp.M**2 / setup.S_beam
    zmin = tau / xiA

    totB = 0.
    if z >= 1:
        return 0

    elif z <= zmin:
        for iParton in range(1, setup.nf+1):

            totB -= setup.sigma0[iParton] / setup.S_beam\
                    * (pdf.pdf(xiA, iParton, Q2, mem)\
                    * pdf.pdf(zmin, -iParton, Q2, mem)\
                    + pdf.pdf(xiA, -iParton, Q2, mem)\
                    * pdf.pdf(zmin, iParton, Q2, mem))\
                    * (np.log(dp.M**2 / Q2) * kernel.Pqq_plus(1) / (1-z)\
                    + kernel.Dq_plus(1) * np.log(1-z) / (1-z))

        return pdf.alpha.alphasQ2(Q2) / np.pi / setup.S_beam * totB

    elif z > zmin:
        for iParton in range(1, setup.nf+1):

            totB += (pdf.pdf(xiA, iParton, Q2, mem)\
                    * pdf.pdf(zmin/z, -iParton, Q2, mem)\
                    + pdf.pdf(xiA, -iParton, Q2, mem)\
                    * pdf.pdf(zmin/z, iParton, Q2, mem))\
                    * setup.sigma0[iParton] / z\
                    * (np.log(dp.M**2/Q2) * kernel.Pqq_plus(z) / (1-z)\
                    + kernel.Dq_plus(z) * np.log(1-z) / (1-z)
                    + kernel.Dq_fin(z))

            totB += (pdf.pdf(xiA, -iParton, Q2, mem)\
                    * pdf.pdf(zmin/z, 0, Q2, mem)\
                    + pdf.pdf(xiA, 0, Q2, mem)\
                    * pdf.pdf(zmin/z, -iParton, Q2, mem)\
                    + pdf.pdf(xiA, iParton, Q2, mem)\
                    * pdf.pdf(zmin/z, 0, Q2, mem)\
                    + pdf.pdf(xiA, 0, Q2, mem)\
                    * pdf.pdf(zmin/z, iParton, Q2, mem))\
                    * (np.log(dp.M**2/Q2) * kernel.Pqg(z) + kernel.Dg(z))\
                    * setup.sigma0[iParton] / z

            totB -= (pdf.pdf(xiA, iParton, Q2, mem)\
                    * pdf.pdf(zmin, -iParton, Q2, mem)\
                    + pdf.pdf(xiA, -iParton, Q2, mem)\
                    * pdf.pdf(zmin, iParton, Q2, mem))\
                    * (np.log(dp.M**2/Q2) * kernel.Pqq_plus(1) / (1-z)\
                    + kernel.Dq_plus(1) * np.log(1-z) / (1-z))\
                    * setup.sigma0[iParton]

        return pdf.alpha.alphasQ2(Q2) / np.pi / setup.S_beam * totB

def second_integrand(xiA, dp, Q2, mem=0, eps=1e-2):
    """Second integral."""

    tau = dp.M**2 / setup.S_beam

    tot = quad(first_integrand, 0., 1.,\
                args=(xiA, dp, Q2, mem),\
                epsrel=eps, epsabs=0, limit=1000)[0]

    for iParton in range(1, setup.nf+1):

        tot += setup.sigma0[iParton] / setup.S_beam\
                * (pdf.pdf(xiA, iParton, Q2, mem)\
                * pdf.pdf(tau/xiA, -iParton, Q2, mem)\
                + pdf.pdf(xiA, -iParton, Q2, mem)\
                * pdf.pdf(tau/xiA, iParton, Q2, mem))\
                * (1+pdf.alpha.alphasQ2(Q2) / np.pi\
                * (np.log(dp.M**2/Q2) * kernel.Pqq_div + kernel.Dq_div))

    return tot / xiA

def integrated_cross_section(dp, scale=1, mem=0, eps=1e-2):
    """Integrated cross section."""

    Q2 = scale * dp.M**2

    tau = dp.M**2 / setup.S_beam

    ics = quad(second_integrand, tau, 1,\
            args=(dp, Q2, mem, eps),\
            epsrel=eps, epsabs=0, limit=1000)[0]

    return ics

def integrated_cross_section_uncertainty(dp, args=None):

    if args['type'] == 'pdf':

        dcs_set = list()
        for m in range(pdf.pset.size):
            dcs_set.append(integrated_cross_section(dp, mem=m))
        dcs = pdf.pset.uncertainty(dcs_set, CL)

        return dcs.central - dcs.errminus, dcs.central + dcs.errplus

    elif args['type'] == 'scale':

        scale = float(args['scale'])
        dcs = [integrated_cross_section(dp, scale=scale**i)\
              for i in np.linspace(-1, 1, args['N'])]

        return float(min(dcs)), float(max(dcs))

    else:
        SyntaxError('No uncertainty parameters provided.')
