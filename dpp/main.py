#!/usr/bin/python2
# -*- coding: utf-8 -*-

import argparse
from dpp import generation
from dpp.logs import log

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--cs", type=str, default="dif")
    parser.add_argument("--mesh", type=str, default="constant")
    parser.add_argument("--methods", type=str, nargs="*", required=True)
    parser.add_argument("--uncertainty", type=str, nargs="*", default="no")
    parser.add_argument("--parametrization", type=str, default="SY")
    parser.add_argument("--pool", type=int, default=6)
    parser.add_argument("--input", type=str, required=True)
    parser.add_argument("--output", type=str, default="output")
    parser.add_argument("--scale", type=str, default="linear")
    parser.add_argument("--mass", type=int, nargs="*")
    parser.add_argument("--range", type=float, nargs=2)
    parser.add_argument("--N", type=int)
    parser.add_argument("--seed", type=int, default=None)


    args = parser.parse_args()

    if args.cs == "dif":
        generation.generation_dcs(args)
    elif args.cs == "int":
        generation.generation_ics(args)
    else:
        text = "The argument \'cs\' is either \'dif\' or \'int\'."
        raise SyntaxError(text)

if __name__ == "__main__":
    main()
