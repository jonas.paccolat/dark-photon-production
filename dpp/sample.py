#!/usr/bin/python2
# -*- coding: utf-8 -*-

import os
import numpy as np
from scipy.interpolate import interp1d
from itertools import product

from dpp import darkphoton
from dpp.distribution import gradation

here = os.path.dirname(os.path.realpath(__file__))

def get_z0(M, from_data=False):
    """
    Get the approximate longitudinal fraction of the differential cross section
    maximum from a data set.
    """

    if from_data is True:
        path = os.path.join(here, 'data', 'z0.txt')
        M_, z0_ = np.loadtxt(path)
        f = interp1d(M_, z0_)

    else:
        A = 1.7696
        B = 0.0047
        f = lambda x: B * x**A

    z0 = f(M)

    return z0

def shrink_longitudinal_interval(zmin, zmax, z0, epsilon):
    """
    Increase the minimal longitudinal fraction and decrease the maximal
    longitudinal fraction for safety reasons.
    """

    dz = z0-zmin
    zmin += epsilon*dz
    zmax -= epsilon*dz

    return zmin, zmax

def get_longitudinal_fraction_limit(M, z0, epsilon=0.1):
    """
    Find the minimal and maximal longitudinal fractions.
    """

    dp = darkphoton.DarkPhoton(M)
    zmin, zmax = dp.get_longitudinal_fraction_limit()
    zmin, zmax = shrink_longitudinal_interval(zmin, zmax, z0, epsilon)

    return zmin, zmax

def get_transversal_fraction_limit(M):
    """
    Find the minimal and maximal transversal momenta.
    """

    dp = darkphoton.DarkPhoton(M)
    kTmax = dp.get_absolute_kTmax()

    return 0, kTmax

def set_zrange(M, mesh, args):
    """
    Compute the shifted gaussian distributed longitudinal interval  .
    """

    z0 = get_z0(M)
    zmin, zmax = get_longitudinal_fraction_limit(M, z0)

    if mesh == "constant":
        zrange = np.linspace(zmin, zmax, args["N"] + 1)

    elif mesh == "gaussian":
        zrange = gradation(zmin, zmax, z0,\
                           (z0 - zmin) * args["sigma_ratio"],\
                           args["N"], args["step"])

    else:
        text = "\'mesh\' must either be \'regular\' or \'gaussian\'."
        raise SyntaxError(text)

    return zrange

def set_kTrange(M, mesh, args):
    """
    Compute the shifted gaussian distributed transverse interval gradation.
    """

    kTmin, kTmax = get_transversal_fraction_limit(M)

    if mesh == "constant":
        kTrange = np.linspace(kTmin, kTmax, args["N"] + 1)

    elif mesh == "gaussian":
        kTrange = gradation(kTmin, kTmax, 0,\
                            args["sigma"],\
                            args["N"], args["step"])

    else:
        text = "\'mesh\' must either be \'regular\' or \'gaussian\'."
        raise SyntaxError(text)

    return kTrange

def init_darkphotons(M, zrange, kTrange):
    """
    Instanciation of the dark photons along the given transverse cut.
    """

    assert len(zrange) == len(kTrange)

    dps = list()
    for z, kT in zip(zrange, kTrange):
        dp = darkphoton.DarkPhoton(M, z, kT)
        if dp.is_in_domain():
            dps.append(dp)

    return dps

def longitudinal_cut(M, kT, mesh, args):

    zrange = set_zrange(M, mesh, args)
    kTrange = np.ones(len(zrange)) * kT
    dps = init_darkphotons(M, zrange, kTrange)

    return dps

def transversal_cut(M, z, mesh, args):

    kTrange = set_kTrange(M, mesh, args)
    zrange = np.ones(len(kTrange)) * z
    dps = init_darkphotons(M, zrange, kTrange)

    return dps

def random_sample(M, sample_size, seed=None):

    dps = list()

    z0 = get_z0(M)
    zmin, zmax = get_longitudinal_fraction_limit(M, z0)
    kTmin, kTmax = get_transversal_fraction_limit(M)

    zs = np.random.uniform(zmin, zmax, sample_size)

    count = 0
    i = 0
    while count < sample_size:

        if i == 0:

            if seed is not None:
                np.random.seed(seed + count)

            zs = np.random.uniform(zmin, zmax, sample_size)
            kTs = np.random.uniform(kTmin, kTmax, sample_size)

        dp = darkphoton.DarkPhoton(M, zs[i], kTs[i])
        if dp.is_in_domain():
            dps.append(dp)
            count += 1

        i = i + 1
        if i == sample_size:
            i = 0

    return dps

def set_Mrange(range, N, scale):

    Mmin, Mmax = range

    if scale == 'linear':
        Mrange = np.linspace(Mmin, Mmax, N)

    elif scale == 'log':
        x = np.linspace(np.log10(Mmin), np.log10(Mmax), N)
        dx = x[1] - x[0]
        x = np.linspace(np.log10(Mmin) + dx/2, np.log10(Mmax) + dx/2, N)
        Mrange = np.power(10, x)

    else:
        text = "\'scale\' must either be \'linear\' or \'log\'."
        raise SyntaxError(text)

    return Mrange

def covering(M, mesh, long_args, tran_args):

    zrange = set_zrange(M, mesh, long_args)
    kTrange = set_kTrange(M, mesh, tran_args)

    grid = {'M': list(), 'z': list(), 'kT': list()}
    for z, kT in product(zrange, kTrange):
        grid['M'].append(M)
        grid['z'].append(z)
        grid['kT'].append(kT)

    return grid
