#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np
import time
from scipy.integrate import quad

from dpp import darkphoton
from dpp import settings
from dpp import pdf as PDF
from dpp import setup as SETUP

pdf = PDF.PDF(settings.pdfset)
setup = SETUP.Setup(settings.P_beam, settings.nf)

### differential cross section

def differential_cross_section(dp, scale=1, mem=0):

    assert dp.kT == 0, 'At LO the dark photon has no transverse momentum.'

    Q2 = scale * dp.M**2

    tot = 0.
    for iParton in range(1, setup.nf+1):

        tot += setup.sigma0[iParton]\
            * (pdf.pdf(dp.xA, iParton, Q2, mem)\
            * pdf.pdf(dp.xB, -iParton, Q2, mem)\
            + pdf.pdf(dp.xA, -iParton, Q2, mem)\
            * pdf.pdf(dp.xB, iParton, Q2, mem))

    prefactor = setup.P_beam / dp.Ek / setup.S_beam

    return prefactor * tot

def differential_cross_section_uncertainty(dp, args=None):

    if args['type'] == 'pdf':

        dcs_set = list()
        for m in range(pdf.pset.size):
            dcs_set.append(differential_cross_section(dp, mem=m))
        dcs = pdf.pset.uncertainty(dcs_set, args['CL'])

        return dcs.central - dcs.errminus, dcs.central + dcs.errplus
    elif args['type'] == 'scale':

        scale = float(args['scale'])
        dcs = [differential_cross_section(dp, scale=scale**i)\
              for i in np.linspace(-1, 1, args['N'])]

        return float(min(dcs)), float(max(dcs))

    else:
        SyntaxError('No uncertainty parameters provided.')

    return

### integrated cross section

def integrand(z, M, scale, mem):

    dp = darkphoton.DarkPhoton(M, z, 0)
    dcs = differential_cross_section(dp, scale, mem)

    return dcs

def integrated_cross_section(dp, scale=1, mem=0, eps=1e-3):

    zmin, zmax = dp.get_longitudinal_fraction_limit()
    args = (dp.M, scale, mem)
    ics = quad(integrand, zmin, zmax, args=args,\
                        epsrel=eps, epsabs=0, limit=1000)[0]

    return ics

def integrated_cross_section_uncertainty(dp, args=None):

    if args['type'] == 'pdf':

        dcs_set = list()
        for m in range(pdf.pset.size):
            dcs_set.append(integrated_cross_section(dp, mem=m))
        dcs = pdf.pset.uncertainty(dcs_set, args['CL'])

        return dcs.central - dcs.errminus, dcs.central + dcs.errplus

    elif args['type'] == 'scale':

        scale = float(args['scale'])
        dcs = [integrated_cross_section(dp, scale=scale**i)\
              for i in np.linspace(-1, 1, args['N'])]

        return float(min(dcs)), float(max(dcs))

    else:
        SyntaxError('No uncertainty parameters provided.')
