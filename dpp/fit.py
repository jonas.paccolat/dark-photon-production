#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from scipy.interpolate import interp1d, griddata

from logs import log
import darkphoton
from sample import get_z0

class FittingError(Exception):
    pass

def fit(data, args):
    """
    Fit the data stored in the multi index dataframe *data*.

    The three methods (css, nlo and div) are fitted as well as their superior
    (sup) and inferior (inf) match.

    Parameters
    ----------
    data: DataFrame
        Multi index dataframe of the differential cross section for one given
        mass.
    args: dict or str
        This variable defines the parameters of the fitting procedure which are
        defined in TODO. If *args* is 'step' ('smooth'), a step (smooth)
        transition between the different domains is implemented. If it is a
        dictionary, the values associated to the provided keys are set instead of
        those of the default step set.

    Returns
    -------
    fits: dict
        Dictionary of the fits associated to each method with keys: 'css',
        'nlo', 'div', 'sup' and 'inf'.
    """

    args = set_arguments(args)
    data_to_fit = prepare_data_to_fit(data, args)
    fits = get_2d_fits(data_to_fit)

    return fits

def set_arguments(args):
    """
    Set the parameters of the fitting procedure.
    """

    if args == 'smooth':
        arguments = {'dkT': 0.01,\
                     'a1': 1, 'a2': 1, 'a3': 0.5,\
                     'b1': 0.3, 'b2': 0.3,\
                     'c1': 10, 'c2': 10}
    elif args == 'step':
        arguments = {'dkT': 0.01,\
                     'a1': 1, 'a2': 1, 'a3': 0.5,\
                     'b1': 0, 'b2': 0,\
                     'c1': 0, 'c2': 0}
    else:
        arguments = {'dkT': 0.01,\
                     'a1': 1, 'a2': 1, 'a3': 0.5,\
                     'b1': 0, 'b2': 0,\
                     'c1': 0, 'c2': 0}
        arguments.update(args)

    return arguments

def prepare_data_to_fit(data, args):
    """
    Fit all transverse cut of the provided data, use the fit to find the superior
    and inferior matching functions and then generate new data over the whole
    (z,kT)-space.

    Parameters
    ----------
    data: DataFrame
        Multi index dataframe of the differential cross section for one given
        mass.
    args: dict or str
        This variable defines the parameters of the fitting procedure which are
        defined in TODO. If *args* is 'step' ('smooth'), a step (smooth)
        transition between the different domains is implemented. If it is a
        dictionary, the values associated to the provided keys are set instead of
        those of the default step set.

    Returns
    -------
    data_to_fit: DataFrame
        Dataframe of the newly generated data associated to each method (css,
        nlo, div, sup and inf).
    """

    data_to_fit = init_dataframe()
    zrange = get_zrange(data)
    for idx, z in enumerate(zrange):
        transverse_data_to_fit = prepare_transverse_data_to_fit(data, idx, args)
        data_to_fit = update_dataframe(data_to_fit, transverse_data_to_fit)

    return data_to_fit

def init_dataframe():
    """
    Initialize the dataframe used to store the newly generated data.
    """

    columns = ['z','kT','sup','inf','css','div','nlo']
    data_to_fit = pd.DataFrame(columns=columns)

    return data_to_fit

def get_zrange(data):
    """
    Get the z coordinates of the data.
    """

    zrange = list(data.loc[0, 'z'])

    return zrange

def update_dataframe(data_to_fit, transverse_data_to_fit):
    """
    Append the newly generated data associated to a given transverse cut to the
    already computed data.
    """

    data_to_fit = data_to_fit.append(transverse_data_to_fit, ignore_index=True)

    return data_to_fit

def prepare_transverse_data_to_fit(data, idx, args):
    """
    Compute all the data (css, nlo, div, sup and inf) associated to transverse
    cut of index *idx*.
    """

    transverse_data = get_transverse_data(data, idx)
    try:
        transverse_fits = get_transverse_fits(transverse_data)
        finer_transverse_data = get_finer_transverse_data(transverse_data,\
                                                          transverse_fits,\
                                                          args['dkT'])
    except FittingError:
        finer_transverse_data = transverse_data

    kT1, kT2 = get_transitions_momenta(finer_transverse_data,\
                                       args['a1'], args['a2'], args['a3'])

    finer_transverse_data = add_matching_fits(finer_transverse_data, kT1, kT2,\
                      args['b1'], args['b2'], args['c1'], args['c2'])

    return finer_transverse_data

def get_transverse_data(data, idx):
    """
    Extract the data associated to the transverse cut of index *idx* from the
    original multi index dataframe.
    """

    idxmax = get_idxmax(data['css'][idx])

    transverse_data = dict()
    transverse_data['z'] = list(data.loc[:idxmax,'z'][idx])
    transverse_data['kT'] = list(data.loc[:idxmax,'kT'][idx])
    transverse_data['css'] = list(data.loc[:idxmax,'css'][idx])
    transverse_data['div'] = list(data.loc[:idxmax,'div'][idx])
    transverse_data['nlo'] = list(data.loc[:idxmax,'nlo'][idx])

    return transverse_data

def get_idxmax(s):
    """
    Get the index of the maximal value of kT for which the differential cross
    section is non zero (in the physical domain) for the provided transverse cut.
    """

    index = list(s.index)
    index.reverse()
    for i in index:
        if not s[i] == 0:
            idxmax = i
            break

    return idxmax


def get_finer_transverse_data(transverse_data, transverse_fits, dkT):
    """
    Generate new data along the considered transverse cut with a finer grid (of
    step *dkT*).
    """

    finer_transverse_data = dict()
    kTmin, kTmax = transverse_data['kT'][0], transverse_data['kT'][-1]
    kTrange = np.arange(kTmin, kTmax, dkT)

    finer_transverse_data['z'] = np.ones(len(kTrange))*transverse_data['z'][0]
    finer_transverse_data['kT'] = kTrange
    finer_transverse_data['css'] = transverse_fits['css'](kTrange)
    finer_transverse_data['nlo'] = transverse_fits['nlo'](kTrange)
    finer_transverse_data['div'] = transverse_fits['div'](kTrange)

    return finer_transverse_data

def get_transverse_fits(transverse_data):
    """
    Compute the fits of the css, nlo and div data along the considered transverse
    cut.
    """

    if len(transverse_data['kT']) <= 3:
        log.warning('kTrange to short for an interpolation.')
        raise FittingError

    else:
        transverse_fits = dict()
        transverse_fits['css'] = interp1d(transverse_data['kT'],\
                                          transverse_data['css'],\
                    kind='linear', bounds_error=False, fill_value=(np.nan,np.nan))
        transverse_fits['div'] = interp1d(transverse_data['kT'][1:],\
                                          transverse_data['div'][1:],\
                    kind='linear', bounds_error=False, fill_value=(np.nan,np.nan))
        transverse_fits['nlo'] = interp1d(transverse_data['kT'][1:],\
                                          transverse_data['nlo'][1:],\
                    kind='linear', bounds_error=False, fill_value=(np.nan,np.nan))

    return transverse_fits

def get_transitions_momenta(finer_transverse_data, a1, a2, a3):
    """
    Find the kT coordinates of the transitions between the three regions.
    """

    status = 'css'
    kT1, kT2 = 0, 0
    for i, kT in enumerate(finer_transverse_data['kT']):

        if status == 'css':
            if finer_transverse_data['css'][i] > finer_transverse_data['nlo'][i]:
                kT1 = kT
                status = 'transition'
        elif status == 'transition':
            if finer_transverse_data['css'][i] < finer_transverse_data['nlo'][i]:
                kT2 = kT
                break
        else:
            raise ValueError('\'status\' is either \'css\' or \'transition\'.')

    if kT2 == 0:
        kT2 = kT1+a3*(finer_transverse_data['kT'][-1]-kT1)

    kT1 = a1*kT1
    kT2 = kT1+a2*(kT2-kT1)

    return kT1, kT2

def add_matching_fits(finer_transverse_data, kT1, kT2, b1, b2, c1, c2):
    """
    Add the superior and inferior matching transverse data of the considered cut.
    """

    sup, inf = get_transverse_matching_data(finer_transverse_data,\
                                            kT1, kT2, b1, b2, c1, c2)
    finer_transverse_data['sup'] = sup
    finer_transverse_data['inf'] = inf

    return pd.DataFrame(finer_transverse_data)


def get_matching_limits(kT1, kT2, kTmax, b1, b2):
    """
    Get the matching domain limits for both transition coordinates (*kT1* and
    *kT2*).
    """

    dkT1 = b1*kT1
    dkT2 = b2*(kT2-kT1)

    kT1a, kT1b = kT1-dkT1, kT1+dkT1
    kT2a, kT2b = kT2-dkT2, kT2+dkT2

    if kT1b > kT2a:
        kT_mean = (kT1b+kT2a)/2
        kT1b, kT2a = kT_mean, kT_mean
        log.warning('Overlap of the first and second matching regions.')

    if kT2b > kTmax:
        kT2b = kTmax
        log.warning('Second matching region upper boundary out of range.')

    return kT1a, kT1b, kT2a, kT2b

def matching_function(kTa, kTb, c):
    """
    Define the transition smoothing function along the transition interval.
    """

    if kTa == kTb:
        r = lambda kT: 1
    else:
        r = lambda kT: 1-np.exp(-c*((kT-kTa)/(kTb-kTa))**2)

    return r

def get_transverse_matching_data(finer_transverse_data,\
                                 kT1, kT2, b1, b2, c1, c2):
    """
    Scan the transverse cut and set the superior and inferior matching values.
    """

    kTmax = finer_transverse_data['kT'][-1]
    kT1a, kT1b, kT2a, kT2b = get_matching_limits(kT1, kT2, kTmax, b1, b2)
    r1 = matching_function(kT1a, kT1b, c1)
    r2 = matching_function(kT2a, kT2b, c2)

    kTrange = finer_transverse_data['kT']
    css = finer_transverse_data['css']
    nlo = finer_transverse_data['nlo']
    div = finer_transverse_data['div']

    sup, inf = list(), list()
    status = 'css'
    for i, kT in enumerate(kTrange):
        if status == 'css':
            sup.append(css[i])
            inf.append(css[i])
            if finer_transverse_data['kT'][i+1] >= kT1a:
                status = 'match1'
        elif status == 'match1':
            sup.append(css[i]+r1(kT)*(nlo[i]-div[i]))
            if kT < kT1:
                inf.append(css[i])
            else:
                inf.append(nlo[i])
            if kTrange[i+1] >= kT1b:
                status = 'transition'
        elif status == 'transition':
            sup.append(css[i]+nlo[i]-div[i])
            inf.append(nlo[i])
            if kTrange[i+1] >= kT2a:
                status = 'match2'
        elif status == 'match2':
            sup.append(nlo[i]+(1-r2(kT))*(css[i]-div[i]))
            inf.append(nlo[i])
            if kTrange[i+1] >= kT2b:
                status = 'nlo'
        elif status == 'nlo':
            sup.append(nlo[i])
            inf.append(nlo[i])
        else:
            text = '\'status\' is either \'css\', \'match1\', '\
                   '\'tansition\', \'match2\' or \'nlo\'.'
            raise ValueError(text)

    return sup, inf

def get_2d_fits(data_to_fit):
    """
    Fit the data for each method (css, nlo, div, sup and inf) over the whole
    (z,kT)-domain and return a dictionary of functions.
    """

    def sup(z, kT):
        dcs = griddata((data_to_fit['z'], data_to_fit['kT']), data_to_fit['sup'], (z, kT),\
                            method='linear', fill_value=0)
        return dcs

    def inf(z, kT):
        dcs = griddata((data_to_fit['z'], data_to_fit['kT']), data_to_fit['inf'], (z, kT),\
                            method='linear', fill_value=0)
        return dcs

    def css(z, kT):
        dcs = griddata((data_to_fit['z'], data_to_fit['kT']), data_to_fit['css'], (z, kT),\
                            method='linear', fill_value=0)
        return dcs

    def nlo(z, kT):
        dcs = griddata((data_to_fit['z'], data_to_fit['kT']), data_to_fit['nlo'], (z, kT),\
                            method='linear', fill_value=0)
        return dcs

    def div(z, kT):
        dcs = griddata((data_to_fit['z'], data_to_fit['kT']), data_to_fit['div'], (z, kT),\
                            method='linear', fill_value=0)
        return dcs

    fits = dict()
    fits['sup'] = sup
    fits['inf'] = inf
    fits['css'] = css
    fits['nlo'] = nlo
    fits['div'] = div

    return fits
