# -*- coding: utf-8 -*-

import os
import sys
import pickle
import pandas as pd
import numpy as np
import time
import json
from functools import partial
from pathos.multiprocessing import ProcessingPool as Pool

from dpp import css, div, nlo, npm
from dpp import sample, darkphoton
from dpp.logs import log

here = os.path.dirname(os.path.realpath(__file__))

def load_parameters(path):

    with open(path) as f:
        parameters = json.load(f)

    return parameters

def collide_protons(dps, parameters, args):
    """
    Compute the differential cross sections of the dark photons in *dps* for
    each method in *methods*.
    """

    dcs = dict()

    if "nlo" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            dcs["nlo"] = p.map(nlo.differential_cross_section, dps)
        else:
            dic = dict()
            for type in args["uncertainty"]:
                para = parameters["uncertainty"][type]
                para["type"] = type
                func = partial(nlo.differential_cross_section_uncertainty,\
                                args=para)
                dic[type] = p.map(func, dps)
            dcs["nlo"] = dic

    if "div" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            dcs["div"] = p.map(div.differential_cross_section, dps)
        else:
            dic = dict()
            for type in args["uncertainty"]:
                para = parameters["uncertainty"][type]
                para["type"] = type
                func = partial(div.differential_cross_section_uncertainty,\
                                args=para)
                dic[type] = p.map(func, dps)
            dcs["div"] = dic

    if "css" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            dcs["css"] = p.map(css.differential_cross_section, dps)
        else:
            if "pdf" in args["uncertainty"]:
                dic = dict()
                para = parameters["uncertainty"]["pdf"]
                para["type"] = "pdf"
                para["parametrization"] = args["parametrization"]
                func = partial(css.differential_cross_section_uncertainty,\
                               args=para)
                dic["pdf"] = p.map(func, dps)
                dcs["css"] = dic

    if "npm" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            dcs["npm"] = p.map(npm.differential_cross_section, dps)
        else:
            dic = dict()
            for type in args["uncertainty"]:
                para = parameters["uncertainty"][type]
                para["type"] = type
                func = partial(npm.differential_cross_section_uncertainty,\
                                args=para)
                dic[type] = p.map(func, dps)
            dcs["npm"] = dic

    return dcs

def save_slice(dps, dcs, args):

    data = pd.DataFrame()

    data['M'], data['z'], data['kT'] = 0, 0, 0
    for idx, dp in enumerate(dps):

        data.loc[idx, 'M'] = dp.M
        data.loc[idx, 'z'] = dp.z
        data.loc[idx, 'kT'] = dp.kT

    if args["uncertainty"] == "no":
        for key in dcs.keys():
            data[key] = 0
            for idx, dcs_ in enumerate(dcs[key]):
                data.loc[idx, key] = dcs_
    else:
        for key1 in dcs.keys():
            for key2 in dcs[key1].keys():
                key = '{} {}'.format(key1, key2)
                data[key] = [(0, 0, 0, 0)] * len(data)
                for idx, dcs_ in enumerate(dcs[key1][key2]):
                    data.at[idx, key] = dcs_

    return data

def init_data(data_z, parameters, args):

    keys = data_z.columns
    index = range(parameters["mesh"][args["mesh"]]["transverse"]["N"] + 1)
    columns = range(parameters["mesh"][args["mesh"]]["longitudinal"]["N"] + 1)
    columns = pd.MultiIndex.from_product([keys, columns])

    if args["uncertainty"] == "no":
        cell_init = 0.
    else:
        cell_init =  [[[]] * len(columns)] * len(index)

    data = pd.DataFrame(cell_init, columns=columns, index=index)

    update_data(data, data_z, 0)

    return data

def update_data(data, data_z, idx):
    """
    Add the new transverse cut data to the dataframe *data* ; in-place.
    """

    for key in data_z.keys():
        for i, x in enumerate(data_z[key]):
            data.loc[i, (key, idx)] = x

def longitudinal_cut(M, kT, parameters, args):

    mesh = args["mesh"]
    dps = sample.longitudinal_cut(M, kT, mesh,\
                    parameters["mesh"][mesh]["longitudinal"])
    dcs = collide_protons(dps, parameters, args)
    data = save_slice(dps, dcs, args)

    return data

def transversal_cut(M, z, parameters, args):

    mesh = args["mesh"]
    dps = sample.transversal_cut(M, z, mesh,\
                    parameters["mesh"][mesh]["transverse"])
    dcs = collide_protons(dps, parameters, args)
    data = save_slice(dps, dcs, args)

    return data

def remove_empty_lists(data):

    for i, x in data.iterrows():
        for key in x.keys():
            if x[key] == []:
                r = data.loc[0, key]
                if type(r) == float:
                    x[key] = 0
                else:
                    x[key] = (0, ) * len(data.loc[0, key])

def regular_sample(M, args):

    parameters = load_parameters(args["input"])
    mesh = args["mesh"]

    zrange = sample.set_zrange(M, mesh,\
                    parameters["mesh"][mesh]["longitudinal"])

    for idx, z in enumerate(zrange):

        data_z = transversal_cut(M, z, parameters, args)

        if idx == 0:
            data = init_data(data_z, parameters, args)
        else:
            update_data(data, data_z, idx)

    if not args["uncertainty"] == "no":
        remove_empty_lists(data)

    if args["output"] is not None:
        data.to_pickle(os.path.join(here, 'data', args["output"]), protocol=2)

    return data

def random_sample(M, args):

    parameters = load_parameters(args["input"])

    sample_size = parameters["mesh"]["random"]["sample_size"]

    dps = sample.random_sample(M, sample_size, seed=args["seed"])
    dcs = collide_protons(dps, parameters, args)

    data = dcs
    data['M'], data['z'], data['kT'] = list(), list(), list()

    for dp in dps:

        data['M'].append(dp.M)
        data['z'].append(dp.z)
        data['kT'].append(dp.kT)

    if args["output"] is not None:
        with open(os.path.join(here, 'data', args["output"]), 'wb') as f:
            pickle.dump(data, f, protocol=2)

    return data

def generation_dcs(args):

    arguments = {"mesh": args.mesh,
                 "methods": args.methods,
                 "uncertainty": args.uncertainty,
                 "parametrization": args.parametrization,
                 "pool": args.pool,
                 "input": args.input,
                 "seed": args.seed}

    if args.mass is None:
        Mrange = sample.set_Mrange(args.range, args.N, args.scale)
    else:
        if len(args.mass) > 1:
            Mrange = args.mass
        else:
            Mrange = sample.set_Mrange([args.mass, args.mass], 1, args.scale)

    outputs = ["{}{}.pkl".format(args.output, i) for i in range(len(Mrange))]

    if args.mesh == "random":

        for M, output in zip(Mrange, outputs):

            arguments["output"] = output

            tic = time.time()
            data = random_sample(M, arguments)
            tac = time.time()

            text = 'Random covering at M={} [GeV] finished in {} seconds.'
            log.info(text.format(M, round(tac - tic, 2)))

    elif args.mesh == "gaussian" or args.mesh == "constant":

        for M, output in zip(Mrange, outputs):

            arguments["output"] = output

            tic = time.time()
            data = regular_sample(M, arguments)
            tac = time.time()

            text = 'Regular covering at M={} [GeV] finished in {} seconds.'
            log.info(text.format(M, round(tac - tic, 2)))

### integrated cross sections

def compute_ics(dps, parameters, args):

    ics = dict()

    if "nlo" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            ics["nlo"] = p.map(nlo.integrated_cross_section, dps)
        else:
            dic = dict()
            for type in args["uncertainty"]:
                para = parameters["uncertainty"][type]
                para["type"] = type
                func = partial(nlo.integrated_cross_section_uncertainty,\
                                args=para)
                dic[type] = p.map(func, dps)
            ics["nlo"] = dic

    if "npm" in args["methods"]:

        p = Pool(args["pool"])
        if args["uncertainty"] == "no":
            ics["npm"] = p.map(npm.integrated_cross_section, dps)
        else:
            dic = dict()
            for type in args["uncertainty"]:
                para = parameters["uncertainty"][type]
                para["type"] = type
                func = partial(nlo.integrated_cross_section_uncertainty,\
                                args=para)
                dic[type] = p.map(func, dps)
            ics["npm"] = dic

    return ics

def generation_ics(args):

    import darkphoton

    arguments = {"methods": args.methods,
                 "uncertainty": args.uncertainty,
                 "pool": args.pool,
                 "input": args.input}

    parameters = load_parameters(arguments["input"])

    if args.mass is None:
        Mrange = sample.set_Mrange(args.range, args.N, args.scale)
    else:
        if len(args.mass) > 1:
            Mrange = args.mass
        else:
            Mrange = sample.set_Mrange([args.mass, args.mass], 1, args.scale)

    tic = time.time()
    dps = [darkphoton.DarkPhoton(M) for M in Mrange]
    data = compute_ics(dps, parameters, arguments)
    data['M'] = Mrange
    tac = time.time()

    with open(os.path.join(here, 'data', "{}.pkl".format(args.output)), 'wb') as f:
        pickle.dump(data, f, protocol=2)

    text = 'Integrated cross section computed for {} masses in {} seconds.'
    log.info(text.format(args.N, round(tac - tic, 2)))
