# -*- coding: utf-8 -*-

"""Top-level package for DarkPhotonProduction."""

__author__ = """Jonas Paccolat"""
__email__ = 'jonas.paccolat@outlook.com'
__version__ = '0.1.0'
