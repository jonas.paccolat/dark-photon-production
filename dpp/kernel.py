#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np

# group constants
CF = 4./3.
TR = 2.
NC = 3

# one-loop evolution kernels
Pqq_plus = lambda x: CF * (1 + x**2)
Pqq_div = 3 * CF / 2
Pqg = lambda x: TR * (x**2 + (1 - x)**2)

# coefficient functions (prescription dependent)
Dq_div = CF * (np.pi**2 / 3 - 4)
Dq_fin = lambda x: -CF * np.log(x) * (1 + x**2) / (1 - x)
Dq_plus = lambda x: 2 * CF * (1 + x**2)
Dg = lambda x: TR / 2\
               * ((x**2 + (1 - x)**2) * np.log((1 - x)**2 / x)\
               + (3 + 2 * x - 3 * x**2) / 2)
