#!/usr/bin/python2
# -*- coding: utf-8 -*-

import os
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d
import fit

here = os.path.dirname(os.path.realpath(__file__))

class DCS:
    """
    Class to compute the different differential cross section fit for a given
    mass.

    The fits are first computed for masses belonging to the dataset and then
    interpolated to the considered mass.

    Parameters
    ----------
    M: float
        Mass of the dark photon in GeV.
    args: dict or str, optional
        This variable defines the parameters of the fitting procedure which are
        defined in TODO. If *args* is 'step' ('smooth'), a step (smooth)
        transition between the different domains is implemented. If it is a
        dictionary, the values associated to the provided keys are set instead of
        those of the default step set.
    filenames: list, optional
        List of the .pkl filenames. Each file contains information relative to
        one mass. If None the standard names (data0.pkl,...,data14.pkl) are
        used. Default is None.
    """

    def __init__(self, M, args='step', filenames=None):
        self.M = M
        self.args = args
        self.interp_masses, self.interp_data = self.get_interpolation_masses_and_data(filenames)
        self.fits = self.init_fits()

    def load_data(self, filenames):
        """Load all the data (for all masses)."""

        if filenames is None:
            filenames = ['data{}.pkl'.format(i) for i in range(15)]

        dataset = [pd.read_pickle(os.path.join(here, 'data', filename))
                for filename in filenames]

        return dataset

    def get_all_masses(self, dataset):
        """List all masses from the dataset."""

        all_masses = [data.loc[0, 'M'][0] for data in dataset]

        return all_masses

    def get_interpolation_masses_and_data(self, filenames):
        """
        Select the three closest masses to the considerd one as well as their
        associated data.
        """

        dataset = self.load_data(filenames)
        all_masses = self.get_all_masses(dataset)

        Mmin = all_masses[0]
        Mmax = all_masses[-1]

        if self.M < Mmin or self.M > Mmax:
            text = 'Provided mass (M={} GeV) out of the considered mass range '\
                   '(M={} GeV to M={} GeV).'
            log.error(text.format(self.M, Mmin, Mmax))

        delta_masses = [np.abs(M - self.M) for M in all_masses]
        indices = np.argsort(delta_masses)[0:3]
        interp_masses = [all_masses[index] for index in indices]
        interp_data = [dataset[index] for index in indices]

        return interp_masses, interp_data

    def init_fits(self):
        """Compute the fits of the three selected masses"""

        fits = [fit.fit(data, self.args) for data in self.interp_data]

        return fits

    def dcs(self, z, kT, method):
        """
        Compute the mass interpolated differential cross section.

        Parameters
        ----------
        z: numpy.array
            Longitudinal fractions.
        kT: numpy.array
            Transverse momenta.
        method: str
            Differential cross section method: 'nlo', 'css', 'div'

        Returns
        -------
        numpy.array
            Differential cross sections.
        """

        Y = np.array([fit[method](z, kT) for fit in self.fits]).T
        dsigma = np.array([interp1d(self.interp_masses, y, kind=2)(self.M)
                           for y in Y])

        return dsigma
