#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np

from dpp.utils import plus_convolution, convolution
from dpp import settings
from dpp import pdf as PDF
from dpp import setup as SETUP

pdf = PDF.PDF(settings.pdfset)
setup = SETUP.Setup(settings.P_beam, settings.nf)

def F_quark(x):
    """
    One loop quark evolution kernel (to be convoluted)
    """

    return (1+x**2)/(1-x)

def F_gluon(x):
    """
    One loop gluon evolution kernel (to be convoluted)
    """

    return ((1-x)**2+x**2)

def G_quarkA(x, dp, iParton, fxA, fxB, Q2, mem):
    """
    Quark PDF terms of configuration A. (to be convoluted)
    """

    # get +/- indces in the fxA basis
    ip = iParton+setup.nf
    im = iParton-1
    tot = fxA[ip]*pdf.pdf(dp.xB/x, -iParton, Q2, mem)\
          +fxA[im]*pdf.pdf(dp.xB/x, iParton, Q2, mem)

    return tot/x

def G_quarkB(x, dp, iParton, fxA, fxB, Q2, mem):
    """
    Quark PDF terms of configuration B. (to be convoluted)
    """

    # get +/- indces in the fxB basis
    ip = iParton+setup.nf
    im = iParton-1
    tot = fxB[ip]*pdf.pdf(dp.xA/x, -iParton, Q2, mem)\
          +fxB[im]*pdf.pdf(dp.xA/x, iParton, Q2, mem)

    return tot/x

def G_gluonA(x, dp, iParton, fxA, fxB, Q2, mem):
    """
    Gluon PDF terms of configuration A. (to be convoluted)
    """

    # get +/- indces in the fxA basis
    ip = iParton+setup.nf
    im = iParton-1
    tot = fxA[ip]*pdf.pdf(dp.xB/x, 0, Q2, mem)\
        + fxA[im]*pdf.pdf(dp.xB/x, 0, Q2, mem)

    return tot/x

def G_gluonB(x, dp, iParton, fxA, fxB, Q2, mem):
    """
    Gluon PDF terms of configuration B. (to be convoluted)
    """

    # get +/- indces in the fxB basis
    ip = iParton+setup.nf
    im = iParton-1
    tot = fxB[ip]*pdf.pdf(dp.xA/x, 0, Q2, mem)\
        + fxB[im]*pdf.pdf(dp.xA/x, 0, Q2, mem)

    return tot/x

def get_fxA(dp, Q2, mem):
    """
    Get the PDF at the xA for each parton.
    """

    fxA = [pdf.pdf(dp.xA, iParton, Q2, mem)\
           for iParton in range(-setup.nf, setup.nf+1)]

    return fxA

def get_fxB(dp, Q2, mem):
    """
    Get the PDF at the xB for each parton.
    """

    fxB = [pdf.pdf(dp.xB, iParton, Q2, mem)\
           for iParton in range(-setup.nf, setup.nf+1)]

    return fxB

def collinear_contribution(dp, fxA, fxB, Q2, mem, eps):
    """
    Collinear contribution to the divergent differential cross section.
    """

    tot = 0.
    for iParton in range(1, setup.nf+1):

        args = (dp, iParton, fxA, fxB, Q2, mem)
        tot += setup.sigma0[iParton]\
               *(plus_convolution(F_quark, G_quarkA, dp.xB, args, eps=eps)\
               + plus_convolution(F_quark, G_quarkB, dp.xA, args, eps=eps)
               + 3./8.*convolution(F_gluon, G_gluonA, dp.xB, args, eps=eps)\
               + 3./8.*convolution(F_gluon, G_gluonB, dp.xA, args, eps=eps))

    return tot

def soft_contribution(dp, fxA, fxB):
    """
    Collinear contribution to the divergent differential cross section.
    """

    tot = 0.
    for iParton in range(1, setup.nf+1):
        ip = iParton+setup.nf
        im = iParton-1
        tot += setup.sigma0[iParton]*\
            (fxA[ip]*fxB[im]+fxA[im]*fxB[ip])
    tot *= 4.*np.log(dp.M/dp.kT)-3.

    return tot

def differential_cross_section(dp, scale=1, mem=0, eps=1e-3):

    if dp.kT == 0:
        return np.nan

    Q2 = scale*dp.M**2
    prefactor = setup.P_beam/dp.Ek*2*pdf.alpha.alphasQ2(Q2)\
                /(3*np.pi*dp.kT*dp.kT*setup.S_beam)

    fxA = get_fxA(dp, Q2, mem)
    fxB = get_fxB(dp, Q2, mem)

    dcs_collinear = collinear_contribution(dp, fxA, fxB, Q2, mem, eps)
    dcs_soft = soft_contribution(dp, fxA, fxB)
    prefactor = setup.P_beam/dp.Ek*2*pdf.alpha.alphasQ2(Q2)\
                /(3*np.pi*dp.kT*dp.kT*setup.S_beam)
    dcs = prefactor*(dcs_collinear+dcs_soft)

    return dcs

def differential_cross_section_uncertainty(dp, args):

    if args['type'] == 'pdf':

        dcs_set = list()
        for m in range(pdf.pset.size):
            dcs_set.append(differential_cross_section(dp, mem=m))
        dcs = pdf.pset.uncertainty(dcs_set, args['CL'])

        return dcs.central - dcs.errminus, dcs.central + dcs.errplus

    elif args['type'] == 'scale':

        scale = float(args['scale'])
        dcs = [differential_cross_section(dp, scale=scale**i)\
              for i in np.linspace(-1, 1, args['N'])]

        return float(min(dcs)), float(max(dcs))

    else:
        SyntaxError('No uncertainty parameters provided.')
