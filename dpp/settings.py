#!/usr/bin/python2
# -*- coding: utf-8 -*-

# PDF set name
pdfset = "CT14nlo"

# Proton beam momentum [GeV]
P_beam = 400

# Number of active flavors
nf = 3
