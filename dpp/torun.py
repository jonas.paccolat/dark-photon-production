import pickle

import darkphoton
import css, nlo, div

if __name__ == "__main__":

    datas = list()
    for i in range(10):

        with open("data/uncertainty/{}.pkl".format(i), 'rb') as f:
            data = pickle.load(f,  encoding='latin1')
            datas.append(data)

    for i, data in enumerate(datas):

        print(i)

        divs = list()
        csss = list()
        nlos = list()

        for j, (M, z, kT) in enumerate(zip(data["M"], data["z"], data["kT"])):

            dp = darkphoton.DarkPhoton(M, z, kT)
            divs.append(div.differential_cross_section(dp))
            csss.append(css.differential_cross_section(dp))
            nlos.append(nlo.differential_cross_section(dp))

        datas[i]["div"]["central"] = divs
        datas[i]["css"]["central"] = csss
        datas[i]["nlo"]["central"] = nlos

        with open("data/uncertainty/{}new.pkl".format(i), 'wb') as f:
            pickle.dump(data, f, protocol=2)
