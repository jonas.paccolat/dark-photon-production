#!/usr/bin/python2
# -*- coding: utf-8 -*-

from scipy.integrate import quad
import numpy as np

def plus_convolution(F, G, a, args, eps=1e-3):
    """
    Convolution of the function G and the "+" distribution F.
    """

    assert 0 <= a < 1, 'the lower bound {} must be in the range [0,1).'.format(a)

    def G_local(x):
        return G(x, *args)

    G_local1 = G_local(1)

    def func(x):
        return F(x)*(G_local(x)-G_local1)

    I1 = quad(func, a, 1, epsrel=eps, epsabs=0, limit=1000)[0]
    I2 = quad(F, 0, a, epsrel=eps, epsabs=0, limit=1000)[0]

    return I1-G_local1*I2

def convolution(F, G, a, args, eps=1e-3):
    """
    Convolution of the functions F and G.
    """

    assert 0 <= a < 1, 'the lower bound {} must be in the range [0,1).'.format(a)

    def func(x):
        return F(x)*G(x, *args)

    I1 = quad(func, a, 1, epsrel=eps, epsabs=0, limit=1000)[0]

    return I1
