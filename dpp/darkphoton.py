#!/usr/bin/python2
# -*- coding: utf-8 -*-

import numpy as np
from dpp.logs import log
from dpp import settings
from dpp import setup as SETUP

setup = SETUP.Setup(settings.P_beam, settings.nf)

class DarkPhoton:

    def __init__(self, M, z=None, kT=None):
        self.M, self.z, self.kT = self._init_parameters1(M, z, kT)
        self.yCM, self.xA, self.xB, self.MT, self.kL, self.Ek = self._init_parameters2()

    def _init_parameters1(self, M, z, kT):
        """
        Initialization of the mass, the longitudinal fraction and the transverse
        momentum.

        If *z=None*, only the mass is set (integrated cross section). If *z* is
        a real number and *kT=None*, *kT* is set to zero (NPM single
        differential cross section ds/dz). Else all three parameters are used.

        Parameters
        ----------
        M: float
            Dark photon mass.
        z: float
            Dark photon longitudinal fraction.
        kT: float
            Dark photon transverse momentum.

        Returns
        -------
        list:
            (M, z, kT)
        """

        # initialization without momenta (for integrated cross section)
        if z is None:
            if kT is not None:
                text = 'No longitudinal fraction is provided. The dark photon '\
                       'is initialized without momentum. The transverse '\
                       'momentum kT={} is neglected.'
                log.info(text.format(kT))
            return M, None, None

        # initialization without transverse momentum (for single NPM
        # differential cross section ds/dz, kT=0)
        elif kT is None:
            return M, z, 0

        # complete initialization
        else:
            return M, z, kT

    def _init_parameters2(self):
        """
        Initialization of the parameters depending on the dark photon kinematics.

        Returns
        -------
        yCM: float
            Rapidity between the laboratory frame and the CM frame. None if
            *DarkPhoton.z=None*.
        xA: float
            First longitudinal fraction limit. None if *DarkPhoton.z=None*.
        xB: float
            Second longitudinal fraction limit. None if *DarkPhoton.z=None*.
        MT: float
            Dark photon transverse mass. None if *DarkPhoton.z=None*.
        kL: float
            Dark photon longitudinal momentum. None if *DarkPhoton.z=None*.
        Ek: float
            Dark photon energy. None if *DarkPhoton.z=None*.
        """

        # cross section (NLO)
        if self.z == None:
            return None, None, None, None, None, None

        # single differential cross section (NPM)
        else:
            MT = np.sqrt(self.M**2 + self.kT**2)
            kL = self.z * setup.P_beam
            Ek = np.sqrt(MT**2 + kL**2)
            yCM = setup.y0 + 0.5*np.log((Ek+kL) / (Ek-kL))
            xA = self.M / np.sqrt(setup.S_beam) * np.exp(yCM)
            xB = self.M / np.sqrt(setup.S_beam) * np.exp(-yCM)

            return yCM, xA, xB, MT, kL, Ek

    def is_in_domain(self, eps=0):
        """
        Returns True if the dark photon momentum lies inside the allowed domain.

        The on-shell condition and the energy condition are checked.
        """

        energy = np.arccosh(np.sqrt(setup.S_beam) / self.MT)
        onshell = self.xA*self.MT/self.M\
                  + self.kT**2/setup.S_beam/(1 - self.xB*self.MT/self.M)

        cond_energy = -energy + eps <= self.yCM <= energy - eps
        cond_onshell = self.M**2 / setup.S_beam + eps <= onshell <= 1 - eps

        return cond_energy*cond_onshell

    def sample(self, sample_size, seed=None):

        kTmin, kTmax = 0, self.get_absolute_kTmax()
        zmin, zmax = self.get_longitudinal_fraction_limit()

        zs = list()
        kTs = list()
        batch = sample_size / 10

        i = 0
        while True:

            np.random.seed(seed + i)
            i += 1

            z = np.random.uniform(zmin, zmax, batch)
            kT = np.random.uniform(kTmin, kTmax, batch)

            MT = np.sqrt(self.M**2 + kT**2)
            kL = z * setup.P_beam
            Ek = np.sqrt(MT**2 + kL**2)
            yCM = setup.y0 + 0.5*np.log((Ek+kL) / (Ek-kL))
            xA = self.M / np.sqrt(setup.S_beam) * np.exp(yCM)
            xB = self.M / np.sqrt(setup.S_beam) * np.exp(-yCM)

            energy = np.arccosh(np.sqrt(setup.S_beam) / MT)
            onshell = xA * MT / self.M \
                      + kT**2 / setup.S_beam / (1 - xB * MT / self.M)

            cond_energy = np.logical_and(-energy <= yCM, yCM <= energy)
            cond_onshell = np.logical_and(self.M**2 / setup.S_beam <= onshell,
                                          onshell <= 1)
            cond = np.logical_and(cond_energy, cond_onshell)

            zs = np.concatenate([zs, z[cond]])
            kTs = np.concatenate([kTs, kT[cond]])

            if len(zs) >= sample_size:
                break

        return zs[:sample_size], kTs[:sample_size]

    def get_kTmax(self):
        """
        Compute the maximal allowed value of the transverse momentum for the
        considered dark photon mass and longitudinal fraction.

        Returns
        -------
        float:
            Maximal kT value.
        """

        kTmax = ((1 + self.M**2/setup.S_beam)\
                * self.M/(self.xA + self.xB))**2\
                - self.M**2

        return np.sqrt(kTmax)

    def get_absolute_kTmax(self):
        """
        Compute the maximal allowed value of the transverse momentum for the
        considered dark photon mass.

        Returns
        -------
        float:
            Absolute maximal kT value.
        """

        absolute_kTmax = (setup.S_beam - self.M**2)\
                         / (2*np.sqrt(setup.S_beam))

        return absolute_kTmax

    def get_longitudinal_fraction_limit(self):
        """
        Compute the minimal and maximal value of the longitudinal fraction for
        the considered dark photon mass.

        Returns
        -------
        list
            (zmin, zmax)
        """

        zmin = self.M**2*np.exp(-setup.y0)\
                / (2*setup.P_beam*np.sqrt(setup.S_beam))\
                - np.sqrt(setup.S_beam)*np.exp(setup.y0)\
                / (2*setup.P_beam)

        zmax = -self.M**2*np.exp(setup.y0)\
                / (2*setup.P_beam*np.sqrt(setup.S_beam))\
                + np.sqrt(setup.S_beam)*np.exp(-setup.y0)\
                / (2*setup.P_beam)

        return zmin, zmax
