#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from dpp import setup

s = setup.Setup(400, 3)

def test_init():

    assert s.P_beam == 400
    assert s.nf == 3

def test_init_energy():

    assert round(s.E_beam, 3) == 400.001
    assert round(s.S_beam, 3) == 752.162

def test_init_born_interaction():

    assert len(s.sigma0) == 7
    assert round(s.sigma0[2], 3) == 0.012

def test_init_cm_energy():

    assert round(s.E_CM, 3) == 13.713
    assert round(s.P_CM, 3) == 13.681

def test_init_relative_rapidity():

    assert round(s.y0, 3) == -3.374
