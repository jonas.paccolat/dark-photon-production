#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from dpp import setup
from dpp import darkphoton

def test__init_parameters1():

    M, z, kT = 2, 0.5, 3
    dp = darkphoton.DarkPhoton(M)

    assert dp._init_parameters1(M, None, None) == (M, None, None)
    assert dp._init_parameters1(M, None, kT) == (M, None, None)
    assert dp._init_parameters1(M, z, None) == (M, z, 0)
    assert dp._init_parameters1(M, z, kT) == (M, z, kT)

def test__init_parameters2():

    M, z, kT = 2, 0.5, 3
    dp1 = darkphoton.DarkPhoton(M)
    dp2 = darkphoton.DarkPhoton(M, z, kT)

    assert dp1._init_parameters2() == (None, None, None, None, None, None)
    assert round(dp2._init_parameters2()[1], 2) == 0.28

def test_is_in_domain():

    dp1 = darkphoton.DarkPhoton(5, 0.2, 2)
    dp2 = darkphoton.DarkPhoton(5, 0.2, 20)

    assert dp1.is_in_domain() == True
    assert dp2.is_in_domain() == False

def test_get_kTmax():

    M, z, kT = 2, 0.5, 3
    dp = darkphoton.DarkPhoton(M, z, kT)

    assert round(dp.get_kTmax(), 2) == 6.49

def test_get_absolute_kTmax():

    M, z, kT = 2, 0.5, 3
    dp = darkphoton.DarkPhoton(M, z, kT)

    assert round(dp.get_absolute_kTmax(), 2) == 13.64

def test_get_longitudinal_fraction_limit():

    M, z, kT = 2, 0.5, 3
    dp = darkphoton.DarkPhoton(M, z, kT)
    zmin, zmax = dp.get_longitudinal_fraction_limit()

    assert round(zmin, 4) == 0.0042
    assert round(zmax, 4) == 1.0012
