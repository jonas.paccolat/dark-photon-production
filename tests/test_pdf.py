#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from dpp import setup
from dpp import pdf
from dpp import settings

PDF = pdf.PDF(settings.pdfset)

def test_init_name():

    assert PDF.pdfset == PDF.pset.name

def test_init_alpha():

    assert PDF.alpha.orderQCD() == 1
    assert round(PDF.alpha.alphasQ2(25), 4) == 0.2122

def test_init_number():

    assert PDF.pset.size == len(PDF.pdfs)

def test_init_value():

    assert PDF.pdfs[0].xfxQ2(1, 0.5, 10) == PDF.pcentral.xfxQ2(1, 0.5, 10)

def test_pdf_conditions():

    assert PDF.pdf(-1, 1, 10, 2) == 0.0
    assert PDF.pdf(2, 1, 10, 2) == 0.0
    assert PDF.pdf(0.5, 1, 10, 2) > 0.0

def test_pdf_value():

    assert PDF.pdf(0.5, 1, 10, 0) == PDF.pcentral.xfxQ2(1, 0.5, 10)/0.5
