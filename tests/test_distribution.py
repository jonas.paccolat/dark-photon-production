#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from dpp import distribution

# stand alone tests
def test_shifted_gaussian_distribution():

    x0 = 0.52
    sigma = 3.2
    a = 2.1
    A = 0.21
    f = distribution.shifted_gaussian_distribution(x0, sigma, a, A)

    assert f(x0) == A*(1+a)

def test_get_shift_constant_from_step():

    a = distribution.get_shift_constant_from_step(0., 1., 0.1, 1., 10, 0.01)

    assert round(a, 2) == -0.92

def test_get_normalization():

    A = distribution.get_normalization(0., 1., 0.1, 1., 10, -0.5)

    assert round(A, 2) == 25.52

def test_real_value_index():

    i = distribution.real_value_index(0.5, 0., 0.1, 1., -0.5, 2)

    assert round(i, 2) == 0.48

def test_get_coordinates():

    xrange = distribution.get_coordinates(0., 0.1, 1., 10, 0.5, 2)

    assert len(xrange) == 11
    assert round(xrange[3], 2) == 1.1

def test_get_astar():

    astar = distribution.get_astar(0., 1., 0.1, 1)

    assert round(astar, 2) == -0.89

def test_get_amin():

    amin = distribution.get_amin(0., 1., 0.1, 1)

    assert round(amin, 2) == -0.67

def test_get_step_limits():

    step_min, step_max = distribution.get_step_limits(0., 1., 0.1, 1, 10)

    assert step_min == 0.1
    assert round(step_max, 2) == 0.28

def test_gradation():

    xrange = distribution.gradation(0., 1., 0.1, 1., 10, 0.2)

    assert len(xrange) == 11
    assert round(xrange[3], 2) == 0.23
    with pytest.raises(distribution.DistributionError):
        distribution.gradation(0., 1., 0.1, 1., 10, 0.01)
