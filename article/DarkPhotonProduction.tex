%
\documentclass[%
 reprint,
superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
%preprint,
%showpacs,
preprintnumbers,
nofootinbib,
%nobibnotes,
%bibnotes,
 amsmath,amssymb, aps,
%pra,
%prb,
%rmp,
%prstab,
%prstper,
%floatfix,
]{revtex4-1}
%\pdfoutput=1
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point

\usepackage{xcolor}


\bibliographystyle{unsrt}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}


\title{Dark photon production, NLO}% Force line breaks with \\


\author{J. Paccolat}
 \email{jonas.paccolat@epfl.ch}
\affiliation{Institute of Physics, Laboratory for Particle Physics and Cosmology,\\
\'{E}cole Polytechnique F\'{e}d\'{e}rale de Lausanne, CH-1015 Lausanne, 
Switzerland}%


\author{I. Timiryasov}
 \email{inar.timiryasov@epfl.ch}
\affiliation{Institute of Physics, Laboratory for Particle Physics and Cosmology,\\
\'{E}cole Polytechnique F\'{e}d\'{e}rale de Lausanne, CH-1015 Lausanne, 
Switzerland}


\begin{abstract}

The SHiP (Search for Hidden Particles) collaboration has proposed a many-purposes experiment, which should gather data from SPS---400 GeV---protons during Run 3. This work extends some previous predictions \cite{SHiP} on the sensitivity of the detector to the production of dark photons. In particular it provides an efficient pythonic tool to compute the hadronic production at next-to-leading order using the CSS resummation formula \cite{Collins1985}. Besides the accuracy improvement from the previous simulations (done at leading order), our work reveals the so far neglected transverse momentum dependence. A discussion on the Bremstrahlung production channel is also presented. 

\end{abstract}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:intro}

In the very broad and debated quest for Dark Matter (DM), a new possibility of collider production (SHiP, NA64, FASER, SeaQuest,...) has emerged as very promising in the last few years. It is hypothesized that the yet non-observation of dark matter is due to its tiny interaction strength. This intensity frontier paradigm is to be opposed to the energy frontier paradigm which assumes very heavy dark matter particles. This last possibility has been intensely probed by large scale experiments (LHC or Tevatron) without success in the past decades. The new paradigm does not only rely on strong theoretical arguments but it is also experimentally attractive for its relatively low costs \cite{SHiP,US}. \\

The above cited collaborations aim at discovering some very weakly interacting long lived particles, such as Heavy Neutral Leptons as well as vector, scalar or axion portals to the Hidden Sector (HS) with small scale beam dump experiments. Our work is restricted to the production of vector portals---so-called dark photons and a particular attention is paid to the SHiP (Search for Hidden Particles) facility which characteristics are considered for the simulations. \\

The paper is organized as follows...


\section{Dark photons} % (fold)
\label{sec:dark_photons}

One possible extension of the Standard Model (SM), which could be probed by the intensity frontier type experiments is the addition of a Hidden Sector (HS) completely neutral under the forces of the SM sector and of a new force which bridges both sectors. Here, it shall be assumed that the force carrier is a vector, even though models with scalar portals, neutrino portals or non-renormalizable portals also lead to promising results. The HS is assumed to possess a $U(1)_{HS}$ gauge group which is coupled to the SM through its kinetic term, namely
\begin{equation*}
    \mathcal{L}=\mathcal{L}_{SM}+\mathcal{L}_{DS}-\frac{\epsilon}{2}F_{\mu\nu}F'_{\mu\nu}\hspace{3pt},
\end{equation*}
where $F_{\mu\nu}$ and $F'_{\mu\nu}$ are respectively the field strength tensor of the $U(1)$ groups of the SM and of the HS. In view of the introduction, the HS quest relies on the assumption that the mixing angle is very small, $\epsilon \ll 1$, while the masses of the dark particles and their couplings remain sizable. We remain agnostic as to the matter composition of the HS Lagrangian and shall simply refer to the dark particles as $\chi$, so that
\begin{equation*}
    \mathcal{L}_{DS}=-\frac{1}{4}(F'_{\mu\nu})^2+\frac{1}{2}M^2(A'_{\mu})^2+\mathcal{L}_{\chi}\hspace{3pt}.
\end{equation*}
The signatures of the existence of a dark photon $A'$ of mass $M$ are mainly sensitive to the two parameters $\epsilon$ and $M$ and, in particular, to the hierarchy between the the dark photon mass and the mass, $m_{\chi}$, of the lightest dark particle charged under $U(1)_{DS}$. Indeed, if such particles exist and are lighter than the vector portal, then the decay channels of the dark photon are highly model dependent and phenomenological predictions less fruitful. However, if $M<2 m_{\chi}$, dark photons must decay into SM particles. It is shown that the decay length of a dark photon of mixing angle $\epsilon\sim10^{-4}-10^{-8}$ and mass $M\sim$ few $100$ MeV is compatible with observations at beam dump experiments such as SHiP \cite{SHiP,SeaQuest}.\\ 


% section dark_photons (end)


\section{Dark photon direct production} % (fold)
\label{sec:dark_photon_direct_production}

What is still debated and motivates this work is the computation of the different production channels. Using the equation of motion $\partial_{\mu}F_{\mu\nu}=e J_{\mu}^{EM}+o(\epsilon)$, the dark photon vertex takes the simple effective form 
\begin{equation}
     \epsilon e A'_{\mu}J_{\mu}^{EM}+o(\epsilon^2)\hspace{3pt},
\end{equation}
where $J_{\mu}^{EM}$ is the electromagnetic current. It appears then that the dark photon may be treated as a massive photon of electromagnetic coupling $\epsilon e$. In the particular case of proton beam dump experiments, the dominant production processes, from higher to lower mass $M$, are the following \cite{SeaQuest} :
\begin{enumerate}
    \item Hard Drell-Yan collision : When the mass of the dark photon exceeds the QCD scale, the dominant production comes from parton interactions. 
    \item Proton Bremsstrahlung : The incident proton radiates a dark photon while it interacts almost elastically with the target.
    \item Meson decay :  In view of the effective dark photon description, secondary mesons decay producing photons may as well be a source of dark photons with the associated $\epsilon^2$ suppression factor.
\end{enumerate}
An accurate determination of the differential cross section of these different production channels is crucial for the design of the emulsion detector of the SHiP experiment. The meson decay channel as already been computed accurately \cite{Pospelov}. The Bremstrahlung process is more delicate and we shall discuss the approach proposed by \cite{Blumlein} in section \ref{sec:a_comment_on_bremsstrahlung}. However, the core of this work is the improvement to next-to-leading order of the hard production channel, which reveals the transverse momentum dependence lacking at leading order. \\

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{Figures/setup.pdf} 
	\caption{SHiP experiment setup and the three dominant production channels depending on the dark photon mass. \textcolor{red}{Mensuration of the experiment ?}.}
	\label{fig:setup}
\end{figure}

% section dark_photon_direct_production (end)

\section{Hard production} % (fold)
\label{sec:hard_production}

In this section we shall compute and compare the naive parton model (NPM) differential cross section \cite{FieldBook}, the next-to-leading order (NLO) integrated cross section \cite{Handbook} and the Collins-Soper-Sterman (CSS) double differential cross section \cite{Collins1985}. In subsection \ref{subsec:theory} the different formalisms are presented for the considered dark photon production. In subsection \ref{subsec:numerical_implementation} their predictions are compared numerically. In particular a \textit{Python} code is used to fit the time consuming CSS computation. 

\subsection{Theory} % (fold)
\label{subsec:theory}

In the inclusive dark photon production the energy scale of the interaction is fixed by the mass $M$ of the vector portal. Hence, for masses above the QCD scale the dominant process is hard in the sense that the dark photon emerges from a direct collision of two parton cascades (short range), while the rest of the protons interact through soft processes (long range). The short range interaction has a perturbative expansion while the long range one is described by phenomenological quantities: the Parton Distribution Funtions (PDF). These describe the probability to find a given parton with a given momentum inside a hadron \cite{Collins2003}.\\

The general expression for the inclusive production of a dark photon $A'$ together with any hadronic state $X$ from two protons $A$ and $B$ is
\begin{multline*}
    d\sigma(AB\to A' X)=\\
    \sum_{a,b}\int_{x_A}^1 d\xi_A\int_{x_B}^1 d\xi_B f_{a/A}(\xi_A,\mu_F)f_{b/B}(\xi_B,\mu_F) d\hat{\sigma}(a b\to A')\hspace{3pt},
\end{multline*}
where the sum runs over any two partons $a$ and $b$ (quark, anti-quark or gluon) and the integral is over the longitudinal fractions of the hadronic momenta $P_{A/B}$ carried away by the partons of respective momenta $p_{a/b}=\xi_{A/B}P_{A/B}$. The very reason of the success of the parton description is the factorization property of the hadronic interaction exemplified in the above expression \citep{Collins1989}. The hadronic interaction is obtained as a weighted sum over all parton configurations of the associated partonic interaction $d\hat{\sigma}$. The weight corresponding to the configuration with parton momenta $p_a$ and $p_b$ is given by the product of PDFs $f_{a/A}(\xi_A,\mu_F)f_{b/B}(\xi_B,\mu_F)$. The factorization scale $\mu_F$ which separates the short range from the long range physics is unphysical. Nevertheless for troncated perturbative expansions the choice of the factorization scale matters \cite{Maltoni}.

\subsubsection{Leading order and kinematics} % fold
\label{subsubsec:leading_order_and_kinematics}

Let's define the parameters in the laboratory frame where the incoming and target proton momenta are
\begin{equation*}
    P_A=\big(E_A,0,0,P\big)\hspace{15pt}\mbox{and}\hspace{15pt}P_B=\big(m,0,0,0\big)\hspace{3pt},
\end{equation*}
with $E_A=\sqrt{m^2+P^2}$ and $m$ the proton mass. The momentum of the dark photon is characterized by the two variables $0<z<1$ and $k_T>0$, namely
\begin{equation}\label{eq:LABmomentum}
    k=\big(E_k,k_T,0,k_L\big)\hspace{3pt},
\end{equation}
with $E_k=\sqrt{M^2+k_T^2+k_L^2}$ and $k_L=z P$. Note that the $x$-axis is defined along the direction of the transverse momentum of the dark photon.\\

At leading order, the dark photon is created from the annihilation of a quark-antiquark pair. This simple vertex  is characterized by the Born amplitude \cite{EllisBook}
\begin{equation}
    \overline{\sum_{pol}}|\mathcal{M}^{LO}|^2=\frac{\sigma_0}{\pi}M^2\hspace{3pt}\mbox{, with}\hspace{3pt}\sigma_0=\frac{4\pi^2}{3}Q_q^2\epsilon^2\alpha\hspace{3pt},
\end{equation}
where $Q_q$ is the charge of quark $q$ in $e$-unit and $\alpha$ is the fine structure constant. In this limit the dark photon remains collinear to the proton beam with a momentum coresponding to the following differential cross section
\begin{equation*}
    \frac{d\sigma^{NPM}}{dz}=\sum_{q}\frac{\sigma_0}{zs}f_{q/A}(x_A)f_{\bar{q}/B}(x_B)\hspace{3pt},
\end{equation*}
where the sum runs over all quarks and antiquarks, $s$ is the center of mass energy and
\begin{equation*}
    x_A=\frac{M}{\sqrt{s}}e^y\hspace{15pt}\mbox{and}\hspace{15pt}x_B=\frac{M}{\sqrt{s}}e^{-y} \hspace{3pt},
\end{equation*}
are kinematic bounds which respect the equality $x_A x_B=M^2/s$. The rapidity in the CM frame is given by $y=\frac{1}{2}\log(x_A/x_B)$. 

%subsubsection leading_order_and_kinematics (end)

\subsubsection{Next-to-leading order} % (fold)
\label{subsubsec:next_to_leading_order}

The computation of the NLO correction is important for two reasons: (1) it improves the accuracy---the K-factor is close to two, and (2) it reveals information on the transverse profile of the differential cross section.\\
At NLO order, diagrams with one additional loop or one additional leg exist. The two families of corrections are represented on figure \ref{fig:}. All the loops are UV divergent and lead to the renormalization of the strong coupling $\alpha_s\to\alpha_s(\mu_R)$. Also and most importantly, all the diagrams are IR divergent. They exhibit soft divergences ($q\to0$) as well as collinear divergences ($q \parallel p_a$ or $q \parallel p_b$). Here $q$ is the momentum of the additional line. These long distance singularities are obviously not physical and merely reflect our poor understanding of the non-perturbative QCD effects. Nevertheless, the collinear divergences can be absorbed into scale dependent PDFs, which hide the actual non-perturbative behaviour behind phenomenological input \cite{EllisBook}. In the following the factorization scale and the renormalization scale are chosen identical $\mu_R=\mu_F$. \\

In addition, when integrating the differential cross section all the remaining soft divergences cancel at each perturbative order. In particular, at next-to-leading order the $\overline{\text{MS}}$ regularisation scheme leads to the following integrated cross section

\begin{widetext}
\begin{multline*}
\sigma^{NLO}(M,\mu^2) = \sum_q \frac{\sigma_0}{s} \int_0^1 \Bigg\{ \Big[ f_{q/A}(\xi_A,\mu^2)f_{\bar{q}/B}(\xi_B,\mu^2) + q \leftrightarrow \bar{q} \Big] \times \Big[ \delta(1-x)+\frac{\alpha_s}{\pi} \Big( \log \frac{M^2}{\mu^2} P_{qq}(x)+D_q(x) \Big)\Big] \\
+ \Big[f_{g/A}(\xi_A,\mu^2)f_{q/B}(\xi_B,\mu^2) + f_{q/A}(\xi_A,\mu^2)f_{g/B}(\xi_B,\mu^2) + q \leftrightarrow \bar{q} \Big] \times \frac{\alpha_s}{\pi} \Big(\log \frac{M^2}{\mu^2} P_{qg}(x)+D_g(x)\Big) \frac{d\xi_A}{\xi_A}\frac{d\xi_B}{\xi_B} dx \delta(\frac{\tau}{\xi_A\xi_B}-x) \Bigg\}
\end{multline*}
\end{widetext}

where $\mu$ is the factorization scale and the sum runs over all quarks. The splitting functions $P$ and the coefficient functions $D$ are given in \cite{Handbook}. These functions are composed of three terms. One proportional to a "plus" distribution, one proportional to a $\delta$-distribution and one finite term. 

However, the differential cross section at small $k_T$ doesn't exhibit such a cancellation of soft divergences. Non-negligible contributions from soft gluon lines occur at each order in pertubation theory. Collins, Soper and Sterman \cite{Collins1985} showed that all these divergences may be resummed into a Sudakov factor. Their CSS method applied to the dark photon production in the laboratory frame reads
\begin{equation}\label{eq:CSS}
    \frac{d\sigma^{CSS}}{dzdk_T^2}=\frac{1}{2z}\int_0^{\infty}db\: b\: J_0(k_T b)\:\mathcal{W}(b,M,x_A,x_B)\hspace{3pt},
\end{equation}
where $J_0$ is the Bessel function of the first kind and 
\begin{equation}\label{eq:W}
   \mathcal{W}(b,M,x_A,x_B)=\text{e}^{-S(b,M)}W(b,x_A,x_B)\hspace{3pt},
\end{equation}
with
\begin{align*}
    S(b,M)&=\int_{b_0^2/b^2}^{M^2} \frac{d\bar{\mu}^2}{\bar{\mu}^2} \Big[\mathcal{A}\big(\alpha_s(\bar{\mu})\big)\log \frac{M^2}{\bar{\mu}^2}+\mathcal{B}\big(\alpha_s(\bar{\mu})\big)\Big]\hspace{3pt},\\
    W(b,x_A,x_B)&=\sum_q \frac{\sigma_0}{s} \mathcal{P}_{q/A}(x_A,b) \mathcal{P}_{\bar{q}/B}(x_B,b)\hspace{3pt}.
\end{align*}
The distributions $\mathcal{P}_{q/A}(x_A,b)$ and $\mathcal{P}_{\bar{q}/B}(x_B,b)$ describe the "probability" that the $q\bar{q}$ pair annihilates with the associated rapidity and a transverse momentum up to $b_0/b$, with $b_0 = 2e^{-\gamma_E}\approx1.12$. Hence the sum is only over quark and antiquark flavors. However, the presence of the quark $q$ in the hard interaction may occur from a series of parton branching triggered by any possible parton $a$ (or $b$) in the parent parton $A$ (or $B$), namely
\begin{equation*}
    \mathcal{P}_{q/A}(x,b)=\sum_a \int_x^1 \frac{d\xi}{\xi} f_{a/A}(x,b_0/b) \mathcal{C}_{qa}\big(\frac{x}{\xi},\alpha_s(b_0/b)\big)\hspace{3pt}.
\end{equation*}
The coefficient functions $\mathcal{C}_{ab}$ are process dependent, while the usual PDFs account for the collinear divergences. Note that the energy scale at which the different quantities are computed varies with the impact parameter $b$, so that a perturbative expansion is only reliable in the range $b \lesssim 1/\Lambda_{QCD}$. In that region one can expand the above resummed cross section to any fixed order and match it with the result obtained in perturbation theory. A detailed computation of $\mathcal{A}$, $\mathcal{B}$ and $\mathcal{C}$ up to NLO can be found in \cite{PerfectDY}. However, in the region $b\gtrsim 1/\Lambda_{QCD}$, a perturbative treatment alone is not sufficient and one must introduce a non-perturbative parametrization \footnote{In the large $M$ limit, the integrand of the differential cross section \eqref{eq:CSS} is dominated by a saddle point in the perturbative region, so that the non-perturbative input can safely be neglected \cite{Collins1985}. The larger the mass the better the saddle point approximation and the smaller its abscissa. Unfortunately, the considered mass scale is too small to justify such an approximation.}. Different phenomenological models have been suggested in the literature \cite{Landry2003}. Here we shall follow \cite{Konychev2006} who gave an improvement of the original idea of \cite{Landry2003}. The idea is to keep the perturbative expressions, but to replace the energy at which they are evaluated by the bounded function
\begin{equation*}
  b^*=\frac{b}{\sqrt{1+b^2/b_{max}^2}}\hspace{3pt},  
\end{equation*}
where the parameter $b_{max}$ separates the perturbative regime from the non-perturbative regime. If we call $\mathcal{W}_{pert}$ the quantity \eqref{eq:W} in the perturbative region, then we may update it over the whole $b$-range to 
\begin{equation*}
    \mathcal{W}(b)=\mathcal{W}_{pert}(b^*)e^{-F_{NP}(b)}\hspace{3pt},
\end{equation*}
where the non-perturbative correction is
\begin{equation*}
    F_{NP}(b)=\big[a_1+a_2 \log \frac{M}{M_0}+a_3\log (100 x_A x_B)\big] b^2\hspace{3pt}.
\end{equation*}
In the small $b$ limit, the perturbative prediction is recovered since $b^*\to b$ and $F_{NP}\to 0$. According to the stability study of \cite{Konychev2006}, the following parameters are used in the simulations: $b_{max}=1.5$ GeV$^{-1}$, $a_1=0.201$ GeV$^2$, $a_2=0.184$ GeV$^2$, $a_3=-0.026$ GeV$^2$ and $M_0=3.2$ GeV. One last comment is needed: according to the definition of $b^*$, the range of energies at which the PDFs will be evaluated along the $b$ integral is $[b_0/b_{max},\infty[$. However, the lower bound of this interval may be smaller than the lower bound $M_{min}$ of the set of PDFs that is used for the numerical computation. In that case, one should use $b_{max}=b_0/M_{min}$ instead of $b_{max}=1.5$ GeV$^{-1}$ for the PDFs energy scale, while keeping everything else unchanged. \\

% subsubsection next_to_leading_order (end)

\subsubsection{Parton dynamics} % (fold)
\label{subsubsec:parton_dynamics}

A thorougher investigation of the parton phase space leads to constraints on the dark photon momenta. The partonic dark photon production is described by four dynamical variables: $\xi_A$, $\xi_B$, $k_T$ and $y$ (or equivalently $z$). Because a parton momentum can't exceed its parent proton momentum and because the parton center of mass energy must allow the dark photon production, the parton longitudinal fractions respect $M^2/s < \xi_A,\xi_B <1$. Also, the on-shell condition of the outgoing parton of mass $\mu$ leads to the constraint \cite{PerfectDY}
\begin{equation*}
	(\xi_A-x_A\frac{M_T}{M})(\xi_B-x_B\frac{M_T}{M}) = \frac{\mu^2+k_T^2}{M^2}
\end{equation*}
with the transverse mass $M_T = \sqrt{M^2+k_T^2}$. The combination of this expression and the domain of the longitudinal fraction translates into
\begin{equation}\label{eq:physical_domain}
\frac{M^2}{s} < x_A \frac{M_T}{M}+\frac{\mu^2+k_T^2}{M^2}\frac{1}{1-x_B M_T/M} < 1
\end{equation}
which sets a boundary in the $(z,k_T)$-plane of the dark photon. (\textcolor{red}{figure ???}). \\

\begin{figure}
	\centering
	\includegraphics[width=0.5\linewidth]{Figures/physical_domain.pdf} 
	\caption{Physical domain in the $(z,kT)$-plane for dark photons of mass $M=1$ GeV (blue), $M=3$ GeV (orange), $M=5$ GeV (green). The fiducial constraint of the SHiP experiment is respected below the dashed line. The gray region correspond to the approximate upper limit of validity of the CSS formula. \textcolor{red}{Make a better figure}}
	\label{fig:physical_domain}
\end{figure}

A few comments are required here. First, the on-shell condition results from the pQCD negligence of power suppressed off-shell lines. Second, the resultant parton mass and energy are positive because the outgoing parton system is the source of hadronic jets. The positive energy condition wasn't discussed because much weaker. Third, at next-to-leading order there is only one outgoing parton, which is assumed massless, so that $\mu = 0$. At higher order, resultant outgoing parton with $\mu > 0$ correspond to stronger constraints on the $(z,k_T)$-plane. Third, at leading order, no outgoing parton is emitted holding the dark photon on the beam axis with momentum in the corresponding above range.


% subsubsection parton_dynamics (end)

% subsection theory (end)

\subsection{Numerical implementation}
\label{subsec:numerical_implementation} 

In this subsection, the CSS integral is implemented numerically. The multiple integrals of the PDFs being highly time consuming, an efficient fit of the differential cross section for the SHiP energy is also provided. The CSS prediction is then compared to the NLO and the NPM predictions. 

\subsubsection{CSS prediction}
\label{subsubsec:CSS_prediction}

We are concerned by the differential cross section over the range given by the physical domain \eqref{eq:physical_domain} tensor multiplied by the mass range $M\in [1,10]$ GeV. The lower mass bound is imposed by the validity of perturbative QCD, while the upper bound is chosen arbitrary, but large enough to step outside of the SHiP sensitivity domain. Also the domain of validity of the CSS formula may be smaller than the physical domain \eqref{eq:physical_domain}. Indeed finite corrections should be accounted for large transverse momenta \cite{}. In practice this is reflected by the presence of negative differential cross sections at large $k_T$ caused by the Bessel function in the integral \eqref{eq:CSS}. The fitting procedure presented below naturally discards these regions. \\

The computation of the CSS prediction \eqref{eq:CSS} deviates from the actual differential cross section for the following reasons:

\begin{enumerate}
\item The perturbative expansion is truncated at next-to-leading order. Higher order perturbative computation for the Drell-Yan process suggest that the relative error is of the order of \textcolor{red}{find good reference}.
\item PDFs are known within a range of uncertainty. We made use of the LHAPDF6 library \cite{LHAPDF6} to access different sets of PDFs. In particular, the NLO CT14 PDFs, provided by \cite{CT14}, were used in the implementation of the CSS formula. This set consists of 57 PDF set members, from which the uncertainties are computed. A 95\% confidence level was chosen. 
\item Partons are assumed massless. As a consequence only the PDFs associted to the three lighter quarks are considered.
\item Numerical errors arise from the multiple integrals of the formula. First, the upper limit of integration $b_{max}=10$ GeV $^{-2}$ was chosen ensuring a per mille accuracy over the whole considered domain. Second, the chosen integral parameters ensures that the propagated numerical error remains below the same precision. 
\end{enumerate}

Two different sets of data are generated over the considered $(M,z,k_T)$-domain, a set of 41176 points without uncertainties and a set of 1068 points with uncertainties. Both set span the domain regularly with an enhancement of points close to the critical regions, namely around the maximum differential cross section for each given mass. The first set is used to fit the differential cross section and the second set to quantify the quality of the fit. The fit is obtained in the following manner: fix the dark photon mass, find the closest three masses from the first set, fit the two dimensional associated differential cross section with the \textit{Python} built-in Clough-Tocher interpolation and interpolate quadratically between the three masses. Results of this procedure are plotted on figure \ref{fig:ds_dzdkT}. The abrupt cuts at small values of $z$ are induced by the mass interpolating algorithm (\textcolor{red}{This can probably be improved}). The cuts at large values of $z$ however correspond to the boundaries of the physical domain represented on figure \ref{fig:physical_domain}. Non-continuity is a reflection of the domain of validity of the CSS formula. Indeed finite corrections should be accounted for at transverse momenta larger than a certain $k_T^{max}(M)$, which increases with $M$. The gray region on figure \ref{fig:physical_domain} is a rough estimate of the limit of validity of the CSS formula. \textcolor{red}{Compute the finite corrections}\\

\begin{figure}
\includegraphics[width=0.5\textwidth]{Figures/dsigma.png}
\label{fig:ds_dzdkT}
\caption{Differential cross section predicted by the CSS method for a dark photon of mass $M=3.25$ GeV. The points and error bars belong to the second set, while the lines correspond to the fit of the first set. \textcolor{red}{Add more masses}}
\end{figure}

The quality of the fit is measured with the $\chi^2$ statistics (\textcolor{red}{I use the definition with variances, namely $\chi^2 = \sum \big( (y_i-f(x_i))/\sigma_i\big)^2$, but most of the literature uses the mean instead...}). This quantity is computed for each dark photon mass of the second set and it remains below $0.1$ over the whole considered mass range. Note that the points from the second set lying below the minimum $z$-value of the first set are removed from the $\chi^2$ computation.\\

\textcolor{red}{Original time consumption to get one point is about 10 seconds. Time to get the fit at a given mass is about $5\cdot 10^{-4}$ seconds and the time to compute a differential cross section from the fit is about $5\cdot 10^{-4}$ seconds. This last performance is obtained for input vectors of more than $10^5$ elements. It can probably be improved with the Numba package. All this values are obtained from my own laptop.}

\subsubsection{Integration and comparison}
\label{subsubsec:integration_and_comparison}

The integrated CSS fit $d\sigma^{CSS}/dz$ and $\sigma^{CSS}$ can be compared to the NPM and NLO predictions. \textcolor{red}{The finite correction of the CSS is still missing.}. The NLO prediction is subject to two main sources of uncertainties: the PDF uncertainty and the factorization scale choice. The first one is computed as for the CSS formula and the second one is obtained by varying $\mu$ from $M/2$ to $2M$. Choosing a scale more precisely than within the previous range cannot be justified physically in the considered $\overline{MS}$ scheme \cite{Maltoni}. Note that this issue is absent of the CSS method, since all factorization scales are explicitly considered. The integrated cross section predicted by the different methods are plotted on figure \ref{fig:s} with the corresponding uncertainties. Figure \ref{fig:ds_dz} compares the CSS and the NPM predictions for the differential cross section $d\sigma/dz$. \textcolor{red}{Discussion}

\begin{figure}
\includegraphics[width=0.5\textwidth]{Figures/sigma.png}
\label{fig:s}
\caption{Integrated cross section}
\end{figure}

\begin{figure}
\includegraphics[width=0.5\textwidth]{Figures/ds_dz.png}
\label{fig:ds_dz}
\caption{}
\end{figure}

% subsection numerical_results (end)

% section hard_production (end)


\section{SHiP sensitivity} % (fold)
\label{sec:ship_sensitivity}

% section ship_sensitivity (end)

\section{A comment on bremsstrahlung} % (fold)
\label{sec:a_comment_on_bremsstrahlung}





% section a_comment_on_bremsstrahlung (end)

\section{Conclusions} % (fold)
\label{sec:conclusions}

% section conclusions (end)

%%%%%%%%%%%%% APPENDIX %%%%%%%%%%%%

\section{Sample distribution} % (fold)
\label{sec:sample_distribution}

As mentioned in section \ref{} in order to best account for the fast amplitude change of the cross section, the sample of fitted dark photons where generated according to a shifted Gaussian distribution along both longitudinal and transverse momenta. This procedure is made explicit in this section.\\

Let's consider the interval $[x_{min},x_{max}]$ which must be split in $N$ steps according to the distribution
\begin{equation*}
	f(x) = A(e^{-(x-x_0)^2/2\sigma^2}+a)
\end{equation*}
where the mean $x_0 \in [x_{min},x_{max}]$ and the variance $\sigma^2$ are fixed a priori, while the shift $a \in \mathbb{R}$ and the normalization $A \in \mathbb{R}$ are adjusted a posteriori to guarantee that the size of the last step equals $dx\equiv dx_N$ and that the number of steps is $N$. We look for the coordinates $x_i$ and their associated steps $dx_i=1/f(x_i)$ for all indices $i=0,1,\dots,N$. Defining the continuous index $\alpha \in [0,N]$, the following integral equation
\begin{equation*}
	\alpha(x) = \int_{x_{min}}^x f(x') dx' = \text{func}(a,A)
\end{equation*} 
relates indices to their coordinates, i.e. if $\alpha=i \in \{0,1,\dots,N\}$ then the integral upper bound $x$ is the coordinate of the $i$st step. The parameters $a$ and $A$ are fixed by the conditions $\alpha(x_{max}) = N$ and $\alpha(x_{max}-dx) = N-1$, leading to 
\begin{align*}
a &= \frac{a^*\Delta x}{\Delta x-N dx} \Big[1-N\frac{erf(\frac{x_{max}-x_0}{\sqrt{2\sigma^2}})-erf(\frac{x_{max}-dx-x_0}{\sqrt{2\sigma^2}})}{E}\Big]\\
A &= \frac{N}{\Delta x (a-a^*)}
\end{align*}
with $\Delta x = x_{max}-x_{min}$, $a*=-\sigma \sqrt{\frac{\pi}{2}}E/\Delta x$ and $E=erf(\frac{x_{max}-x_0}{\sqrt{2\sigma^2}})+erf(\frac{x_0-x_{min}}{\sqrt{2\sigma^2}})$.\\

All sets of parameters won't lead to a meaningful distribution. Two conditions must be met:
\begin{enumerate}
	\item $A>0$, or equivalently $a>a^*$, which is satisfied for all $dx \in (\Delta x/N, \Delta x)$. 
	\item $f(x_{min})\geq 0$ and $f(x_{max})\geq 0$, which is satisfied for $a \geq a_{min}=-e^{-\delta z^2/2\sigma^2}$, with $\delta z = \text{max}(z_0-z_{min}, z_{max}-z_0)$. The associated $dx_{max}$ is found by solving $a(dx_{max}) = a_{min}$.
\end{enumerate}
Hence, a consistent distribution is obtained for $dx \in (\Delta x/N, \text{min}(\Delta x, dx_{max}))$, as illustrated in figure \ref{fig:distribution}.

\begin{figure}
\includegraphics[width=0.4\textwidth]{Figures/distribution.pdf}
\label{fig:distribution}
\caption{Above: Shift and normalization of the distribution function depending on the choice of the last step size $dx$. The other parameters are $x_{min}=0$, $x_{max}=10$, $x_0=2$, $\sigma=4$ and $N=100$. Only the region where the blue curve is larger than the dashed line corresponds to admissible values of $dx$. Below: Representation of the distributed $N$ coordinates for three different values of $dx$.}
\end{figure}


% section sample_distribution (end)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage

\bibliography{references}

\end{document}
